import axios from "axios";


const apiCalls = {
    getImageItemsPreview() {
        return axios.get("/images/stocks/items");
    },
    getImageItems(shortName) {
        return axios.get("/images/stocks/items/" + shortName)
    },
    getImageItemsByModelId(modelId) {
        return axios.get("/images/stocks/data/model/" + modelId)
    },
    getImageItemByImageId(imageId) {
        return axios.get("/images/stocks/data/" + imageId)
    }
}

export default apiCalls;