import {createWebHistory, createRouter} from "vue-router";
import HelloWorld from "@/components/HelloWorld";
import TheFrontPage from "@/page/TheFrontPage";
import TheStocksListPage from "@/page/TheStocksListPage";
import TheStockPage from "@/page/TheStockPage";

const routes = [
    {
        path: "/",
        name: "Index",
        component: TheFrontPage
    },
    {
        path: "/hello",
        name: "About",
        component: HelloWorld
    },
    {
        path: "/stocks",
        name: "stocks",
        component: TheStocksListPage
    },
    {
        path: "/stocks/:stockId",
        name: "stock",
        component: TheStockPage
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;