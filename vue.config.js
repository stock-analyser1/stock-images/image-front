module.exports = {
    devServer: {
        proxy:{
            "^/images": {
                target: "http://localhost:9091/images",
                changeOrigin: true,
                logLevel: "debug",
                pathRewrite: {"^/images": ""}
            }
        }
    }
}